#!/bin/bash
# change raxml executable name to the raxml executable name that you have on your system. There
# are a variety of executables depending on your system architecture. You can also simply soft link
# your favorite executable to 'raxml' and make no changes to this script. 
# You may wish to increase threads used on raxml, tblastn and PaPaRa steps if you have
# multiple cores available. Note that PaPaRa must be compiles to support multiple cores.
# Some executables called with options set for expediency of running the tutorial, but more stringent settings recommended for real enviornmental data. 
# More stringent settings are commented out below those which are set to be executed by this script. 

cd AA/
muscle -in rpsD_AA.fasta -out rpsD_AA_aln.fasta
phan-fastaToPhyml.py -f rpsD_AA_aln.fasta
mkdir ProteinModel
cp rpsD_AA_aln.phymlAln ./ProteinModel/
cd ProteinModel
ProteinModelSelection.pl rpsD_AA_aln.phymlAln > BestModel.txt
cp rpsD_AA_aln.phymlAln.reduced ../
cd ../
raxml -m PROTCATLG -s rpsD_AA_aln.phymlAln.reduced -# 1 -T 8 -p 610 -n rpsD_AA
#raxml -m PROTGAMMALG -s rpsD_AA_aln.phymlAln.reduced -# 20 -T 8 -p 610 -n rpsD_AA
cd ../blastDB/artificialMG/
makeblastdb -parse_seqids -hash_index -dbtype nucl -in artificialMG.fasta -out artificialMG
cd ../../AA/
tblastn -db_gencode 11 -db ../blastDB/artificialMG/artificialMG -outfmt 5 -num_threads 8 -max_target_seqs 1000000 -evalue 1 -query rpsD_AA_seeds_for_blast.fasta -out rpsD_artificialMG.tblastn.xml
phan-parseBlastXML.py -f rpsD_artificialMG.tblastn.xml -n 0 -e ../blastDB/artificialMG/artificialMG -s -l -o rpsD_artificialMG_blastResults -c 8
phan-trimFormatReads.py -g rpsD -f rpsD_artificialMG_blastResults.hits -m 100 -e 1e-5
papara -t RAxML_bestTree.rpsD_AA -s rpsD_AA_aln.phymlAln.reduced -q rpsD_AA_translation_stops_removed.fasta -n rpsD_AA_artificialMG -j 1 -a -f
phan-spaceJoin.py -p papara_alignment.rpsD_AA_artificialMG -r 64 -o rpsD_AA_cut40 -l 40
raxml -f v -m PROTCATLG -s rpsD_AA_cut40_joinedReadsForPlacement.phymlAln -r RAxML_bestTree.rpsD_AA -n rpsD_AA_placements -T 8
#raxml -f v -m PROTGAMMALG -s rpsD_AA_cut40_joinedReadsForPlacement.phymlAln -r RAxML_bestTree.rpsD_AA -n rpsD_AA_placements -T 8
phan-distributions.py -i RAxML_classificationLikelihoodWeights.rpsD_AA_placements -l -o rpsD_likelihoods
phan-distributions.py -i RAxML_classification.rpsD_AA_placements -b -o rpsD_branchLengths
phan-figTreeFiles.py -i RAxML_labelledTree.rpsD_AA_placements -o rpsD_figTree
phan-taxaDictionary.py -O RAxML_originalLabelledTree.rpsD_AA_placements -N rpsD_taxa_dictionary.txt
if [ -f "rpsD_taxa_dictionary.txt_Taxa_Dictionary.txt" ]; then mv rpsD_taxa_dictionary.txt_Taxa_Dictionary.txt rpsD_taxa_dictionary.txt; fi #Appears some versions of taxaDictionary need to have output files renamed. 
phan-treeLabels.py -D rpsD_taxa_dictionary.txt -C RAxML_classification.rpsD_AA_placements
phan-sortPlacements.py -C RAxML_classification.rpsD_AA_placements_placement_list.csv -L Sample_list_IDs.txt
mkdir ../Results
mkdir ../Results/Quantitation
mkdir ../Results/EPA-Out
mkdir ../Results/Vis-Distributions
cp RAxML_labelledTree.rpsD_AA_placements RAxML_originalLabelledTree.rpsD_AA_placements RAxML_classification.rpsD_AA_placements RAxML_classificationLikelihoodWeights.rpsD_AA_placements ../Results/EPA-Out/
cp rpsD_branchLengths_distribution_branch-lengths.html rpsD_likelihoods_distribution_likelihoodWeights.html ../Results/Vis-Distributions/
cp AnnotationsFile_PlacementTree_rpsD_figTree.tab PlacementTree_rpsD_figTree.nw ../Results/
cp RAxML_classification.rpsD_AA_placements_placement_list.csv RAxML_classification.rpsD_AA_placements_placement_list_SMPLA_counts.csv RAxML_classification.rpsD_AA_placements_placement_list_SMPLA_readsPlaced.csv RAxML_classification.rpsD_AA_placements_placement_list_SMPLB_counts.csv RAxML_classification.rpsD_AA_placements_placement_list_SMPLB_readsPlaced.csv ../Results/Quantitation/
