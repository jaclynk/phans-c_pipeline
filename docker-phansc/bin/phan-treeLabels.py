#!/usr/bin/env python

#This file is part of the PHANS-C pipeline.

#the PHANS-C pipeline is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#The PHANS-C pipeline is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with the PHANS-C pipeline.  If not, see <http://www.gnu.org/licenses/>.

#Copyright: University of Washington 2017
#Author: Jaclyn K Saunders

import os
import os.path
from optparse import OptionParser

usage= """
Takes the dictionary file created from the original labelled tree of only Ref taxa and the list of QS w/ their associated placement nodes & categorizes

usage: %prog [-D FILE] [-C FILE] """

parser = OptionParser(usage=usage, version="%prog 0.1")

parser.add_option("-D", "--dictionary file", dest="dictionaryFile",
                  help="Specify the file that contains the node location '[I#]' with the associated Ref taxa name",
                  metavar="FILE")
parser.add_option("-C", "--Classification file", dest="listFile",
                  help="Specify the output file from RAxML-EPA that is a list of the Query sequences assigned to their locations on the Ref Tree",
                  metavar="FILE")

(options, args) = parser.parse_args()

#Makes sure all mandatory options appear
mandatories = ["dictionaryFile", "listFile"]
for m in mandatories:
	if not options.__dict__[m]:
		print("A mandatory option is missing!\n See the HELP menu - 'treeLabels.py -h'") 
		parser.print_help()
		exit(-1)

#Set variables based on collected input
dictFile = os.path.abspath(options.dictionaryFile)
queryFile = os.path.abspath(options.listFile)

outputFileName = queryFile + "_placement_list.csv"

dictOpen = open(dictFile, 'r')

dictRead = dictOpen.readlines()

nodeDict = {}

for line in dictRead:
	line = line.strip("\n")
	listLine = line.split("\t")
	nodeLoc = listLine[0]
	taxaName = listLine[1]
	nodeDict[nodeLoc] = taxaName
	
queryOpen = open(queryFile, 'r')

placementFile = open(outputFileName, "w")

header = "Tree_node, Read_ID\n"
placementFile.write(header)

queryRead = queryOpen.readlines()

for line in queryRead:
	listLine = line.split(" ")
	QSNode = listLine[1]
	QSRead = listLine[0]
	outputFormat = nodeDict[QSNode] + "," + QSRead
	placementFile.write(outputFormat)
	placementFile.write("\n")
	
placementFile.close()
dictOpen.close()
queryOpen.close()