#!/usr/bin/env python

#This file is part of the PHANS-C pipeline.

#the PHANS-C pipeline is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#The PHANS-C pipeline is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with the PHANS-C pipeline.  If not, see <http://www.gnu.org/licenses/>.

#Copyright: University of Washington 2017
#Author: Jaclyn K Saunders

import sys
import os
import re
import os.path
import shutil
from optparse import OptionParser

usage= """
Takes the sequence info file from MicrobesOnline and the associated Sequence file where sequences are identified by VIMS ID and replaces with taxa name: _# in the new sequence name refers to the # of the taxa in the Info file

usage: %prog [-I FILE] [-S FILE]  """

parser = OptionParser(usage=usage, version="%prog 0.1")

parser.add_option("-I", "--Info file", dest="Info_file",
                  help="Specify the file that contains the Sequence info from MicrobesOnline",
                  metavar="FILE")
parser.add_option("-S", "--Sequence file", dest="Fasta_file",
                  help="Specify the sequence file from MicrobeOnline",
                  metavar="FILE")

(options, args) = parser.parse_args()   

#Makes sure all mandatory options appear
mandatories = ["Info_file", "Fasta_file"]
for m in mandatories:
        if not options.__dict__[m]:
                print("A mandatory option is missing!\n")
                parser.print_help()
                exit(-1)

if options.Info_file is not None:
	info_file = os.path.abspath(options.Info_file)
	fasta_file = os.path.abspath(options.Fasta_file)
	(file_path, file_name) = os.path.split(fasta_file)
	(file_name, file_ext) = os.path.splitext(file_name)

	new_file_name = str(file_name) + "_named_reference_taxa" + str(file_ext)
	
	new_file = open(new_file_name, "w")
	name_dict = {}
	
	info_file_open = open(info_file, "r")
	info_file_read = info_file_open.readline()
	info_file_read = info_file_open.readlines()
	
	for line in info_file_read:
		split = line.split(",")
		key = split[0]
		name = split[2]
		name_dict[key] = name
		
	fasta_open = open(fasta_file, "r")
	fasta_read = fasta_open.readlines()
	
	i = 1
	for line in fasta_read:
		if line.startswith(">") is True:
			i = i + 1
			id = line.replace(">","")
			id = id.strip("\n")
			id = id.strip("\r")
			new_file.write(">")
			taxa = name_dict[id]
			taxa = taxa.replace("\"", "")
			taxa = taxa.replace("-", "_")
			taxa = taxa.replace(".","")
			taxa = taxa.replace(" ","_")
			taxa = taxa.replace("'","_")
			taxa = taxa.replace("+","_")
			taxa = taxa.replace(":","_")
			taxa = taxa.replace(";","_")
			taxa = taxa.replace("(","")
			taxa = taxa.replace(")","")
			taxa = taxa.replace("[","")
			taxa = taxa.replace("]","")
			taxa = taxa.replace("/", "")
			taxa = taxa.replace("\\", "")
			new_file.write(taxa)
			index = str(i)
			num = "_" + index + "\n"
			new_file.write(num)
		else:
			new_file.write(line)
	new_file.close()	