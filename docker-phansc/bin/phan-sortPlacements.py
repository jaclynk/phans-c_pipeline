#!/usr/bin/env python

#This file is part of the PHANS-C pipeline.

#the PHANS-C pipeline is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#The PHANS-C pipeline is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with the PHANS-C pipeline.  If not, see <http://www.gnu.org/licenses/>.

#Copyright: University of Washington 2017
#Author: Jaclyn K Saunders

import os
import os.path
from optparse import OptionParser
import re

usage= """
Takes the classification file where all reads are identified by labeled node - if reads from multiple datasets present, use -L option which will sort reads according to unique read identifiers. Unique read identifiers must be one word without special characters - the unique keys are uploaded in a separate file where there is one key per line.

usage: %prog [-L FILE] [-C FILE] """

parser = OptionParser(usage=usage, version="%prog 0.1")


parser.add_option("-L", "--location list", dest="listFile",
                  help="Specify the metagenomic sample unique identifiers - one ID per line without special characters",
                  metavar="FILE")
parser.add_option("-C", "--classifcation File", dest="classificationFile",
                  help="Specify the reads listed according to their node which has already been relabeled by taxaDict",
                  metavar="FILE")
(options, args) = parser.parse_args()

#Makes sure all mandatory options appear
mandatories = ["classificationFile"]
for m in mandatories:
	if not options.__dict__[m]:
		print("A mandatory option is missing!\n See the HELP menu - 'sortPlacements.py -h'") 
		parser.print_help()
		exit(-1)

#Set variables based on collected input
classFile = os.path.abspath(options.classificationFile)
(file_path, file_name) = os.path.split(classFile)
(file_name, file_ext) = os.path.splitext(file_name)

classRead = open(classFile, 'r')

#Create dictionary from classification csv
placeDict = {}

classRead.readline() #skip header
for line in classRead.readlines():
	line = line.strip("\n").split(",")
	treeNode = line[0]
	readID = line[1]

	if treeNode in list(placeDict.keys()):
		oldEntry = list(placeDict[treeNode])
		test = readID
		newEntry = oldEntry + [readID]
		del placeDict[treeNode]
		placeDict[treeNode] = newEntry
	else:
		placeDict[treeNode] = [readID]
allNodes = list(placeDict.keys())		
classRead.close()


#Pull sample ID's for multiple samples
if options.listFile is not None:
	#queryFile = os.path.abspath(options.listFile)
	sampleIDFile = os.path.abspath(options.listFile)
	sampleRead = open(sampleIDFile, 'r').readlines()
	sampleIDlist = []
	for line in sampleRead:
		line = line.strip("\n")
		sampleIDlist.append(line)
else: #Only one sample present
	sampleIDlist = [""]

##Generate output files and parse reads according to samples
for element in sampleIDlist:
	outputPlacementName = file_name + "_" + str(element) + "_counts.csv"
	outputFile = open(outputPlacementName, "w")
	outputFile.write("Tree_node, Total Reads\n")
	outputListName = file_name + "_" + str(element) + "_readsPlaced.csv"
	outputList = open(outputListName, "w")
	outputList.write("Tree_node, Reads_placed\n")

	for key in allNodes:
		reads = placeDict[key]
		sampleSpec = [x for x in reads if x.startswith(element)]
		if len(sampleSpec) > 0:
			numReads = len(sampleSpec)
			outputFile.write(str(key) + "," + str(numReads) + "\n")
			outputList.write(str(key) + "," + ','.join(sampleSpec) + "\n")

outputFile.close()
outputList.close()