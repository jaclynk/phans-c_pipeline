.. PHANS-C documentation master file, created by
   sphinx-quickstart on Mon Nov 20 14:23:41 2017.

PHANS-C Pipeline Documentation: 
===================================

.. image:: PHANS-C_icon.png
   :width: 100pt
   :align: center

Welcome to the **PHANS-C Pipeline**: PHylogenetic Assignment of Next generation Sequences - in the Cloud.

PHANS-C performs inference-based placement analysis of targeted protein coding genes in amino acid space in very large metagenomic datasets, with a focus on differentiating between closely related organisms. PHANS-C can also be used in nucleotide space, with metatranscriptomic sequencing data, and for analysis of broad taxonomic placement. The PHANS-C pipeline tools presented here are implemented in Python 2.7 on Linux based OS and work in conjunction with previously published open source software. 

The complete pipeline can be found on Amazon Web Services (AWS) us-east-1 Amazon Machine Image (AMI) **ami-2125175b**. This AMI has all of the PHANS-C scripts installed, along with all additional software cited and dependencies.



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Overview
   Tutorial
   AWS
   Local



License
-------

The PHANS-C pipeline software is registered under a GPL license, copyright University of Washington 2017.