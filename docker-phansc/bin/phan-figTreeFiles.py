#!/usr/bin/env python

#This file is part of the PHANS-C pipeline.

#the PHANS-C pipeline is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#The PHANS-C pipeline is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with the PHANS-C pipeline.  If not, see <http://www.gnu.org/licenses/>.

#Copyright: University of Washington 2019
#Author: Jaclyn K Saunders

import sys
import os
import re
import os.path
import shutil
from optparse import OptionParser
import pandas as pd

usage= """
Takes the classification tree produced by RAxML-EPA ("RAxML_labelledTree...") and converts to a newick-like file
which can be viewed with the software package FigTree (http://tree.bio.ed.ac.uk/software/figtree/). Also produces
a basic annotation file which can be uploaded into FigTree to color-code read placements.

usage: %prog [-i FILE] [-o STR]"""

parser = OptionParser(usage=usage, version="%prog 0.1")

parser.add_option("-i", "--inputFile", dest="InputFile",
                  help="Specify the placement tree file as input \"RaxML_labelledTree...\"",
                  metavar="FILE")
parser.add_option("-o", "--outfile", dest="outfile",
                  help="specify prefix for output file",
                  metavar="STR")

(options, args) = parser.parse_args()   

#Makes sure all mandatory options appear
mandatories = ["InputFile", "outfile"]
for m in mandatories:
        if not options.__dict__[m]:
                print("A mandatory option is missing!\n")
                parser.print_help()
                exit(-1)

inFile = os.path.abspath(options.InputFile)

treeFile = open(inFile, "r").read().strip("\n")

queries = re.findall(r"(QUERY__\w+):", treeFile)

### Rearrange so that tree file can be viewed in FigTree. 
### "Labels" are internal node labels
### Extant nodes have node label appended to end of name

p = re.compile(r'(?P<extantnode>\w+)?(?P<break>:)(?P<distance>[0-9]+\.[0-9]+)(?P<node>\[I[0-9]*\])')
namedInternalNodes = p.sub(r'\1 \4\2\3', treeFile)

### This differentiates between placed reads and extant nodes
### for generating a FigTree annotations file.

outExtantList = []
attrb1 = []
for m in re.finditer('(?P<query>QUERY__\w+):|(?P<extantnode>\w+)?(?P<break>:)(?P<distance>[0-9]+\.[0-9]+)(?P<node>\[I[0-9]*\])', treeFile):
    taxa = m.group('extantnode')
    node = m.group('node')
    query = m.group('query')
    if taxa != None:
        outStr = str(taxa) + " " + str(node)
        outExtantList.append(outStr)
        attrb1.append("Reference")
    elif query != None:
        outExtantList.append(query)
        attrb1.append("Placement")

outTreeName = "PlacementTree_" + options.outfile + ".nw"
outTree = open(outTreeName, "w")
outTree.write(namedInternalNodes)
outTree.close()

annotationsName = "AnnotationsFile_PlacementTree_" + options.outfile + ".tab"
df = pd.DataFrame(list(zip(outExtantList, attrb1)), columns=["Terminal Nodes", "Category"])
df["Original Order"] = df.index
df.to_csv(annotationsName, sep="\t", index=False)