#!/usr/bin/env python

#This file is part of the PHANS-C pipeline.

#the PHANS-C pipeline is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#The PHANS-C pipeline is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with the PHANS-C pipeline.  If not, see <http://www.gnu.org/licenses/>.

#Copyright: University of Washington 2019
#Author: Jaclyn K Saunders

import sys
import os
import re
import os.path
import shutil
from optparse import OptionParser
from bokeh.io import show, output_file
from bokeh.models import ColumnDataSource, HoverTool, LinearColorMapper, BoxSelectTool
from bokeh.palettes import Spectral6
from bokeh.plotting import figure
from bokeh.transform import factor_cmap
from bokeh.models.widgets import DataTable, TableColumn
from bokeh.layouts import row, layout
from bokeh.transform import transform
import numpy as np
import pandas as pd

usage= """
Takes output from RAxML EPA placements and creates visualized graph of the distribution of
placement likelihoods or branch lengths. Displays table of placements in comma delimited format
that can be copy/pasted for further exploration or read placement quality.

usage: %prog [-i FILE] [-b OPTION] [-l OPTION] [-o STR]"""

parser = OptionParser(usage=usage, version="%prog 0.1")

parser.add_option("-i", "--inputFile", dest="InputFile",
                  help="""Specify the file that contains either final placements & branch lengths(RAxML_classification...)
                  or , the likelihoods of potential placement locations (RAxML_classificationLikelihoodWeights...)""",
                  metavar="FILE")
parser.add_option("-b", "--branchLengths", dest="branchLengths", action="store_true",
                  help="Indicate the option to graph distribution of branch lengths. This is the default. Use with RAxML_classification file.",
                  metavar="OPTION")
parser.add_option("-l", "--likelihoods", dest="likelihoods", action="store_true",
                  help="""Indicate the option to graph distribution of likelihoods for final placement locations. 
                  Use with RAxML_classificationLikelihoodWeights file.""",
                  metavar="OPTION")
parser.add_option("-o", "--outfile", dest="outfile",
                  help="specify prefix for output file",
                  metavar="STR")

(options, args) = parser.parse_args()   

#Makes sure all mandatory options appear
mandatories = ["InputFile", "outfile"]
for m in mandatories:
        if not options.__dict__[m]:
                print("A mandatory option is missing!\n")
                parser.print_help()
                exit(-1)

inFile = os.path.abspath(options.InputFile)

if options.likelihoods is True:

	if "LikelihoodWeights" not in str(inFile):
		print("""\nWARNING: If graphing likelihood distribution, make sure you are using RAxML_classificationLikelihoodWeights as input file!
Check your input file.
Exiting...\n""")
		exit()

	### Open input data as dataframe
	outfileName = options.outfile + "_distribution_likelihoodWeights.html"
	output_file(outfileName)
	mldata = pd.read_csv(inFile, delimiter=" ", names=["read", "node", "ind_ML", "sum_ML"] )
	topml = mldata.groupby(["read"], sort=False)['ind_ML'].max() #Select likelihood score for final placement node
	outml = topml.reset_index()

	### Sort reads/placement likelihoods into bins
	ML_01 = outml.loc[outml["ind_ML"] <= 0.1]
	ML_02 = outml.loc[(outml["ind_ML"] > 0.1) & (outml["ind_ML"] <= 0.2)]
	ML_03 = outml.loc[(outml["ind_ML"] > 0.2) & (outml["ind_ML"] <= 0.3)]
	ML_04 = outml.loc[(outml["ind_ML"] > 0.3) & (outml["ind_ML"] <= 0.4)]
	ML_05 = outml.loc[(outml["ind_ML"] > 0.4) & (outml["ind_ML"] <= 0.5)]
	ML_06 = outml.loc[(outml["ind_ML"] > 0.5) & (outml["ind_ML"] <= 0.6)]
	ML_07 = outml.loc[(outml["ind_ML"] > 0.6) & (outml["ind_ML"] <= 0.7)]
	ML_08 = outml.loc[(outml["ind_ML"] > 0.7) & (outml["ind_ML"] <= 0.8)]
	ML_09 = outml.loc[(outml["ind_ML"] > 0.8) & (outml["ind_ML"] <= 0.9)]
	ML_10 = outml.loc[(outml["ind_ML"] > 0.9) & (outml["ind_ML"] <= 1.0)]

	ML = [ML_01, ML_02, ML_03, ML_04, ML_05, ML_06, ML_07, ML_08, ML_09, ML_10]

	### Arrange into lists for conversion to CDS
	counts = []
	reads_per_ML = []
	for e in ML:
	    length = len(e)
	    counts.append(length)
	    readsStr = e["read"].astype(str).to_list()
	    reads_per_ML.append(readsStr)
	    
	catNames = ["0-0.1 ", "0.1-0.2 ", "0.2-0.3 ", "0.3-0.4 ", "0.4-0.5 ", "0.5-0.6 ", "0.6-0.7 ", "0.7-0.8 ", "0.8-0.9 ", "0.9-1 "]
	colors = ['#8dd3c7', '#ffffb3', '#bebada', '#fb8072', '#80b1d3', '#fdb462', '#b3de69', '#fccde5', '#d9d9d9', '#bc80bd']

	### Convert data into CDS to allow for Bokeh interactivity
	source = ColumnDataSource(data=dict(catNames=catNames, reads=reads_per_ML, counts=counts, colors=colors))

	hover = HoverTool(tooltips=[
	    ("Likelihood Bin Range", "@catNames"),
	    ("# Reads in Bin", "@counts"),
	])

	### Generate Figure
	p = figure(x_range=catNames, plot_height=250, toolbar_location="right", title="Likelihood Distribution of Placed Reads",
	           tools=[hover, "reset", "tap", "box_select", "pan", "wheel_zoom"])

	p.vbar(x='catNames', top='counts', width=0.9, source=source, 
	       line_color='black', alpha=1.0, color="colors") 

	columns = [
	    #TableColumn(field="catNames", title="Likelihood Range", width=10),
	    TableColumn(field="reads", title="Reads Placed")
	]

	data_table = DataTable(source=source, columns=columns, width=400, height=280, index_position=None)

	p.xgrid.grid_line_color = None
	p.y_range.start = 0
	p.y_range.end = max(counts) + 0.1*max(counts)
	p.xaxis.axis_label = "Likelihood Bin"
	p.yaxis.axis_label = "Count"

	l = layout([p, data_table], sizing_mode='stretch_both')
	show(l)

else:

	if "LikelihoodWeights" in str(inFile):
		print("""\nWARNING: If graphing branch length distribution, make sure you are using RAxML_classification as input file!
Check your input file.
Exiting...\n""")
		exit()

	outfileName = str(options.outfile) + "_distribution_branch-lengths.html"
	output_file(outfileName)

	branchData = pd.read_csv(inFile, delimiter=" ", names=["read", "node", "sum_ML", "branch_length"] )

	branchData["reads"] = branchData["read"] + ","
	branchData["binRange"] = pd.cut(branchData["branch_length"], bins=20, right=False)
	gb_branch = branchData[["reads", "binRange"]].groupby("binRange").sum()
	gb_branchdf = gb_branch.reset_index()
	gb_branchdf["Count"] = gb_branchdf["reads"].str.count(",")
	gb_branchdf["Count"] = gb_branchdf["Count"].fillna(0)
	gb_branchdf["binRange"] = gb_branchdf["binRange"].astype("str")
	gb_branchdf["Count"] = gb_branchdf["Count"].astype("int")

	binRange = gb_branchdf["binRange"].to_list()
	reads = gb_branchdf["reads"].to_list()
	counts = gb_branchdf["Count"].to_list()
	index = gb_branchdf.index.to_list()

	src = ColumnDataSource(data=dict(reads=reads, counts=counts, binRange=binRange, index=index))

	hover = HoverTool(tooltips=[
	    ("Branch Length Bin Range", "@binRange"),
	    ("# Reads in Bin", "@counts"),
	])

	b = figure(x_range=binRange, plot_height=250, toolbar_location="right", title="Branch Length Distribution of Placed Reads",
	           tools=[hover, "reset", "tap", "box_select", "pan", "wheel_zoom"])

	mapper = LinearColorMapper(palette='Plasma256', low=min(index), high=max(index))

	b.vbar(source = src, top='counts', x='binRange', width=0.8, #fill_color="navy",
	           line_color="white", alpha=1, fill_color=transform('index', mapper))

	b.y_range.start = 0
	b.xaxis.major_label_orientation = "vertical"
	b.xaxis.axis_label = "Branch Length Bin"
	b.yaxis.axis_label = "Count"

	columns = [
	    TableColumn(field="reads", title="Placed Reads", width=20)
	]

	data_table = DataTable(source=src, columns=columns, width=400, height=280, index_position=None)

	#show(row(m, data_table))
	k = layout([b, data_table], sizing_mode='stretch_both')
	show(k)