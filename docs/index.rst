.. PHANS-C documentation master file, created by
   sphinx-quickstart on Mon Nov 20 14:23:41 2017.

PHANS-C Pipeline Documentation: 
===================================

.. image:: PHANS-C_icon.png
   :width: 100pt
   :align: center

Welcome to the **PHANS-C Pipeline**: PHylogenetic Assignment of Next generation Sequences - in the Cloud.

PHANS-C performs inference-based placement analysis of targeted protein coding genes in amino acid space in very large metagenomic datasets, with a focus on differentiating between closely related organisms. PHANS-C can also be used in nucleotide space, with metatranscriptomic sequencing data, and for analysis of broad taxonomic placement. The PHANS-C pipeline tools presented here are implemented in Python 3.6 on Linux based OS and work in conjunction with previously published open source software. 

The complete pipeline can be found on Amazon Web Services (AWS) us-west-2 Amazon Machine Image (AMI) PHANS-C v 1.0.2 **ami-09bbe25d9d7608ea5**. This AMI has all of the PHANS-C scripts installed, along with all additional software cited and dependencies.

Original PHANS-C Pipeline code can be found at `Bitbucket <https://bitbucket.org/jaclynk/phans-c_pipeline>`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Overview
   Get PHANS-C <Get_PHANS-C>
   Tutorial



License
-------

The PHANS-C pipeline software is registered under a GPL license, copyright University of Washington 2019.

This material is based upon work supported by the National Science Foundation under Grant Numbers OCE-1138368 and OCE-1356779 as well as a NASA Graduate Research Fellowship and NASA Postdoctoral Research Fellowship to JKS.  Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation or the National Aeronautics and Space Administration.

