#!/usr/bin/env python

#This file is part of the PHANS-C pipeline.

#the PHANS-C pipeline is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#The PHANS-C pipeline is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with the PHANS-C pipeline.  If not, see <http://www.gnu.org/licenses/>.

#Copyright: University of Washington 2017
#Author: Jaclyn K Saunders

import sys
import os
import re
import csv
import os.path
import shutil
from optparse import OptionParser

usage= """
Takes a the original labeled tree of only the reference tree taxa and generates a dictionary.
 \n Creates a separate file so that you can open the original labelled tree\nin Dendroscope 
 and add taxa labels to intermediate nodes

usage: %prog [-O FILE] [-N STR]"""

parser = OptionParser(usage=usage, version="%prog 0.1")

parser.add_option("-O", "--RAxML file", dest="tree_file",
                  help="Specify the original tree file output from RAxML-EPA that is the labeled tree by node of Ref taxa only",
                  metavar="FILE")
parser.add_option("-N", "--Output file name", dest="output",
                  help="Specify the output file name for the taxa dictionary")                  

(options, args) = parser.parse_args()

if options.tree_file is None:
	print("\nAn input file is missing!\n")
	parser.print_help()
	exit(-1)



#Set variables based on collected input
taxa_file = os.path.abspath(options.tree_file)

if options.output is None:
	output_file_name = taxa_file + "_Taxa_Dictionary.txt"
else:
	output_file_name = options.output + "_Taxa_Dictionary.txt"
	

####This code takes the original labelled tree file and creates a text file that can be easily converted into a dictionary
####pulls out the node locations [I#] and their corresponding taxa (if they are leaves). 
####Creates a separate file so that you can open the original labelled tree in Dendroscope and add taxa labels to intermediate nodes
taxa_open = open(taxa_file, 'r')

taxa_read = taxa_open.readlines()

taxa_dictionary = open(output_file_name, "w")

for line in taxa_read:
	taxa_reads = ''.join(line)
	line_space = taxa_reads.replace("(", " ").replace(")", " ").replace(","," ")
	ref_seq = re.findall("[A-Za-z_0-9]*:[0-9]+\.[0-9]{20,20}\[I[0-9]+\]", line_space)

	for element in ref_seq:
		list_taxa_place = re.findall("[A-Za-z]+[A-Za-z_0-9]+", element)
		if len(list_taxa_place) >1:
			
			node_loc = list_taxa_place[1]
			taxa_name = list_taxa_place[0]
			taxa_dictionary.write(node_loc)
			taxa_dictionary.write("\t")
			taxa_dictionary.write(taxa_name)
			taxa_dictionary.write("\n")

		elif len(list_taxa_place) == 1:
			node_loc = list_taxa_place[0]
			NA_taxa = list_taxa_place[0]
			taxa_dictionary.write(node_loc)
			taxa_dictionary.write("\t")
			taxa_dictionary.write(node_loc)
			taxa_dictionary.write("\n")

taxa_dictionary.close()
taxa_open.close()