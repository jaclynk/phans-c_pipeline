#!/usr/bin/env python

#This file is part of the PHANS-C pipeline.

#the PHANS-C pipeline is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#The PHANS-C pipeline is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with the PHANS-C pipeline.  If not, see <http://www.gnu.org/licenses/>.

#Copyright: University of Washington 2017
#Author: Jaclyn K Saunders

import os.path
from optparse import OptionParser

usage= """
Takes a fasta alignment file - formats the fasta to phymlAln while also removing any weird characters or spaces from the fasta description line which will break RAxML

usage: %prog [-F FILE] """

parser = OptionParser(usage=usage, version="%prog 0.1")

parser.add_option("-f", "--FASTA file", dest="fastaFile",
                  help="Specify the FASTA in-file that contains the sequence alignment",
                  metavar="FILE")

(options, args) = parser.parse_args()    

if options.fastaFile is None:
	print("\nMust specify a fasta alignment input file\n")
	parser.print_help()
	exit(-1)


fastaFile = os.path.abspath(options.fastaFile)
(file_path, file_name) = os.path.split(fastaFile)
(file_name, file_ext) = os.path.splitext(file_name)

newFileName = str(file_name) + ".phymlAln"
newFile = open(newFileName, "w")
fastaFile = open(fastaFile, "r")

readInfo = []

index = 0

for line in fastaFile:
	line = line.strip("\n")
	if line.startswith(">"):
		if index == 0:
			readID = line[1:]
			sequence = ''
			index += 1

		elif index > 0:
			readData = [readID, sequence]
			readInfo.append(readData)
			readID = line[1:]
			sequence = ''

	elif len(line) > 1:
		sequence = sequence + line

readData = [readID, sequence]
readInfo.append(readData)

numSequences = len(readInfo)
lengthCheck = len(readInfo[0][1])

taxaNameLen = []

for element in readInfo:
	taxaNameLen.append(len(element[0]))

	if len(element[1]) != lengthCheck:
		print("\nERROR: Check your alignment fasta file -- sequences are not of equal length!")
		exit(-1)

headerOut = "  " + str(numSequences) + " " + str(lengthCheck) + "\n"
newFile.write(headerOut)

for element in readInfo:
	taxaSpaces = (max(taxaNameLen) - len(element[0])) + 1
	numSpaces = " "* taxaSpaces
	outline = str(element[0]) + numSpaces + str(element[1]) + "\n"
	newFile.write(outline)

newFile.close()
fastaFile.close()