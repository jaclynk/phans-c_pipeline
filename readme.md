Welcome to the **PHANS-C Pipeline**: **PH**ylogenetic **A**ssignment of **N**ext generation **S**equences - in the **C**loud.

The purpose of this pipeline is to facilitate the use of phylogenetic inference based methods for the classification of short sequence reads. This pipeline can be implemented using cloud based compute resources or locally installed. 

The complete pipeline can be found on Amazon Web Services (AWS) us-west-2 Amazon Machine Image (AMI) `ami-09bbe25d9d7608ea5`. This AMI has all of the PHANS-C scripts installed, along with all additional software cited and dependencies.

A complete tutorial for running the PHANS-C Pipeline can be found at [PHANS-C Pipeline readthedocs.io](http://phans-c-pipeline.readthedocs.io/en/latest/index.html#).

The PHANS-C pipeline relies upon multiple other academic software distributions. The PHANS-C pipeline software is registered under a GPL license, copyright University of Washington 2017-2019.

Please cite the following papers if using this pipeline:

**PHANS-C Pipeline**
Saunders, J. K., McKay, C., Rocap, G. (*submitted*). PHANS-C Pipeline: PHylogenetic Assignment of Next generation Sequences - in the Cloud.

**RAxML**:
Stamatakis, A. (2014). RAxML version 8: a tool for phylogenetic analysis and post-analysis of large phylogenies. Bioinformatics, 30(9), 1312-1313. 

**PaPaRa**:
Berger, S. A., & Stamatakis, A. (2012). PaPaRa 2.0: a vectorized algorithm for probabilistic phylogeny-aware alignment extension. Heidelberg Institute for Theoretical Studies, http://sco.h-its.org/exelixis/publications.html.Exelixis-RRDR-2012-2015. 

**RAxML Evolutionary Placement Algorithm (EPA)**:
Berger, S. A., Krompass, D. & Stamatakis, A. (2011). Performance, Accuracy, and Web Server for Evolutionary Placement of Short Sequence Reads under Maximum Likelihood. Systematic Biology (60) 291-302. doi:10.1093/sysbio/syr010

**BLAST**:
Altschul, S. F., Madden, T. L., Schaffer, A. A., Zhang, J., Zhang, Z., Miller, W., & Lipman, D. J. (1997). Gapped BLAST and PSI-BLAST: a new generation of protein database search programs. Nucleic Acids Res, 25(17), 3389-3402. 

**MUSCLE**:
Edgar, R. C. (2004). MUSCLE: multiple sequence alignment with high accuracy and high throughput. Nucleic Acids Research, 32(5), 1792-1797. doi: 10.1093/nar/gkh340

**Biopython**:
Cock, P. J. A., Antao, T., Chang, J. T., Chapman, B. A., Cox, C. J., Dalke, A., . . . de Hoon, M. J. L. (2009). Biopython: freely available Python tools for computational molecular biology and bioinformatics. Bioinformatics, 25(11), 1422-1423. doi: 10.1093/bioinformatics/btp163

---

This material is based upon work supported by the National Science Foundation (NSF) under Grant Numbers OCE-1138368 and OCE-1356779 and through a National Earth and Space Science Graduate Fellowship (NESSF) and Postdoctoral Program Fellowship (NPP) to JKS from the National Aeronautics and Space Administration (NASA).  Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation or the National Aeronautics and Space Administration.

